# Neoway
Para baixar o exe do terraform, acesse: https://www.terraform.io/downloads.html

O processo de execução do terraform deve ser o seguinte:
1 - Processo de inicialização
$ terraform init 

2 - Verificar o que será executado 
$ terraform plan

3 - Executar o que foi planejado
$ terraform apply

4 - Destruir o que foi executado
$ terraform destroy 

Done
