#Teste DS#

# Existem algumas linhas que devem ser alteradas, todas elas estão comentadas. 
#Linhas a serem alteradas: 8,9,10 e 44.

#KEY, PROVIDER AND REGION 
provider "aws" {
  access_key =  "sua_access_key" #COLOQUE AQUI SUA ACCESS KEY
  secret_key =  "sua_secret_key" #COLOQUE AQUI SUA SECRET KEY
  region = "us-east-1" #COLOQUE AQUI A REGIAO ONDE VC DESEJA SUBIR OS RECURSOS
}

# VPC CREATE 

resource "aws_vpc" "vpc_neoway" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags {
    Name = "neoway-vpc"
  }
}

# SUBNET CREATE

resource "aws_subnet" "sub_neoway" {
  vpc_id     = "${aws_vpc.vpc_neoway.id}"
  cidr_block = "10.0.1.0/24"

  tags {
    Name = "sub_neoway"
  }
}

# SG CREATE

resource "aws_security_group" "SGneoway" {
  vpc_id     = "${aws_vpc.vpc_neoway.id}"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] #colocar o IP de origem de acesso
  }

    egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "SGneoway"
  }

}

#SG EMR CREATE
resource "aws_security_group" "SGneowayEmr" {
  vpc_id     = "${aws_vpc.vpc_neoway.id}"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.0.1.0/24"] 
  }

    egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "SGneowayEmr"
  }

}

# INTERNET GATEWAY CREATE

resource "aws_internet_gateway" "gw_neoway" {
  vpc_id = "${aws_vpc.vpc_neoway.id}"

  tags {
    Name = "gw_neoway"
  }
}

# ROUTE TABLE CREATE 

resource "aws_default_route_table" "rt_neoway" {
  default_route_table_id = "${aws_vpc.vpc_neoway.default_route_table_id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gw_neoway.id}"
  }

}

# ROUTE TABLE ASSOCIATION CREATE

resource "aws_route_table_association" "rts_neoway" {
  subnet_id      = "${aws_subnet.sub_neoway.id}"
  route_table_id = "${aws_default_route_table.rt_neoway.id}"
}


#S3 CREATE

resource "aws_s3_bucket" "b" {
  bucket = "bucketneoway-log"
  force_destroy = "true"
  acl = "log-delivery-write"

  tags {
    Name = "bucketneoway-log"
  }
}


#IAM PROFILE
resource "aws_iam_instance_profile" "emr_profile" {
  name  = "emr_profile"
  roles = ["${aws_iam_role.iam_emr_profile_role.name}"]
}


#EMR CREATE
resource "aws_emr_cluster" "emr_neoway" {
  name          = "EmrNeoway"
  release_label = "emr-5.19.0"
  applications  = ["Spark"]


  ec2_attributes {
    subnet_id                         = "${aws_subnet.sub_neoway.id}"
    emr_managed_master_security_group = "${aws_security_group.SGneowayEmr.id}"
    emr_managed_slave_security_group  = "${aws_security_group.SGneowayEmr.id}"
    instance_profile                  = "${aws_iam_instance_profile.emr_profile.arn}"
  }

  master_instance_type = "m3.xlarge"
  core_instance_type   =  "m3.xlarge"
  core_instance_count  = "1"

  bootstrap_action {
    path = "s3://elasticmapreduce/bootstrap-actions/run-if"
    name = "runif"
    args = ["instance.isMaster=true", "echo running on master node"]
  }

  configurations_json = <<EOF
  [
    {
      "Classification": "hadoop-env",
      "Configurations": [
        {
          "Classification": "export",
          "Properties": {
            "JAVA_HOME": "/usr/lib/jvm/java-1.8.0"
          }
        }
      ],
      "Properties": {}
    },
    {
      "Classification": "spark-env",
      "Configurations": [
        {
          "Classification": "export",
          "Properties": {
            "JAVA_HOME": "/usr/lib/jvm/java-1.8.0"
          }
        }
      ],
      "Properties": {}
    }
  ]
EOF

  service_role = "${aws_iam_role.iam_emr_service_role.arn}"
}

resource "aws_vpc" "main" {
  cidr_block           = "168.31.0.0/16"
  enable_dns_hostnames = true

  tags {
    name = "emr_sub_SGneoway"
  }
}

resource "aws_subnet" "main" {
  vpc_id     = "${aws_vpc.main.id}"
  cidr_block = "168.31.0.0/20"

  tags {
    name = "emr_sub_SGneoway"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.main.id}"
}

resource "aws_route_table" "r" {
  vpc_id = "${aws_vpc.main.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gw.id}"
  }
}

resource "aws_main_route_table_association" "a" {
  vpc_id         = "${aws_vpc.main.id}"
  route_table_id = "${aws_route_table.r.id}"
}


# IAM ROLE FOR EMR SERVICE
resource "aws_iam_role" "iam_emr_service_role" {
  name = "iam_emr_service_role"

  assume_role_policy = <<EOF
{
	"Version": "2008-10-17",
	"Statement": [{
		"Sid": "",
		"Effect": "Allow",
		"Principal": {
			"Service": "elasticmapreduce.amazonaws.com"
		},
		"Action": "sts:AssumeRole"
	}]
}
EOF
}

#IAM ROLE POLICY
resource "aws_iam_role_policy" "iam_emr_service_policy" {
  name = "iam_emr_service_policy"
  role = "${aws_iam_role.iam_emr_service_role.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [{
        "Effect": "Allow",
        "Resource": "*",
        "Action": [
            "ec2:AuthorizeSecurityGroupEgress",
            "ec2:AuthorizeSecurityGroupIngress",
            "ec2:CancelSpotInstanceRequests",
            "ec2:CreateNetworkInterface",
            "ec2:CreateSecurityGroup",
            "ec2:CreateTags",
            "ec2:DeleteNetworkInterface",
            "ec2:DeleteSecurityGroup",
            "ec2:DeleteTags",
            "ec2:DescribeAvailabilityZones",
            "ec2:DescribeAccountAttributes",
            "ec2:DescribeDhcpOptions",
            "ec2:DescribeInstanceStatus",
            "ec2:DescribeInstances",
            "ec2:DescribeKeyPairs",
            "ec2:DescribeNetworkAcls",
            "ec2:DescribeNetworkInterfaces",
            "ec2:DescribePrefixLists",
            "ec2:DescribeRouteTables",
            "ec2:DescribeSecurityGroups",
            "ec2:DescribeSpotInstanceRequests",
            "ec2:DescribeSpotPriceHistory",
            "ec2:DescribeSubnets",
            "ec2:DescribeVpcAttribute",
            "ec2:DescribeVpcEndpoints",
            "ec2:DescribeVpcEndpointServices",
            "ec2:DescribeVpcs",
            "ec2:DetachNetworkInterface",
            "ec2:ModifyImageAttribute",
            "ec2:ModifyInstanceAttribute",
            "ec2:RequestSpotInstances",
            "ec2:RevokeSecurityGroupEgress",
            "ec2:RunInstances",
            "ec2:TerminateInstances",
            "ec2:DeleteVolume",
            "ec2:DescribeVolumeStatus",
            "ec2:DescribeVolumes",
            "ec2:DetachVolume",
            "iam:GetRole",
            "iam:GetRolePolicy",
            "iam:ListInstanceProfiles",
            "iam:ListRolePolicies",
            "iam:PassRole",
            "s3:CreateBucket",
            "s3:Get*",
            "s3:List*",
            "sdb:BatchPutAttributes",
            "sdb:Select"
        ]
    }]
}
EOF
}

# IAM ROLE FOR EC2 INSTANCE PROFILE
resource "aws_iam_role" "iam_emr_profile_role" {
  name = "iam_emr_profile_role"

  assume_role_policy = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "iam_emr_profile_policy" {
  name = "iam_emr_profile_policy"
  role = "${aws_iam_role.iam_emr_profile_role.id}"

  policy = <<EOF
{
	"Version": "2012-10-17",
	"Statement": [{
		"Effect": "Allow",
		"Resource": "*",
		"Action": [
			"cloudwatch:*",
			"dynamodb:*",
			"ec2:Describe*",
			"elasticmapreduce:Describe*",
			"elasticmapreduce:ListBootstrapActions",
			"elasticmapreduce:ListClusters",
			"elasticmapreduce:ListInstanceGroups",
			"elasticmapreduce:ListInstances",
			"elasticmapreduce:ListSteps",
			"kinesis:CreateStream",
			"kinesis:DeleteStream",
			"kinesis:DescribeStream",
			"kinesis:GetRecords",
			"kinesis:GetShardIterator",
			"kinesis:MergeShards",
			"kinesis:PutRecord",
			"kinesis:SplitShard",
			"rds:Describe*",
			"s3:*",
			"sdb:*"

		]
	}]
}
EOF
}
