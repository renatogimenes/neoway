$ scheduler-cli create-period --name "weekdays" --begintime 07:00 --endtime 19:00 --weekdays mon-fri --stack Scheduler
{
   "Period": {
      "Name": "weekdays", 
      "Endtime": "19:00", 
      "Type": "period", 
      "Begintime": "07:00", 
      "Weekdays": [
         "mon-fri"

      ]
   }
}
